package com.example.appbasefragment.models

data class CountryModel(
    val name: String? = null,
    val region: String? = null,
    val nativeName:String? = null,
    val flag:String? = null
)


