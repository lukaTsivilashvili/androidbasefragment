package com.example.appbasefragment.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.appbasefragment.databinding.CountryItemBinding
import com.example.appbasefragment.extensions.loadSvg
import com.example.appbasefragment.models.CountryModel


class RecyclerAdapter :
    RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    val countries: MutableList<CountryModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        CountryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind()
    }


    override fun getItemCount() = countries.size

    fun setData(countries: MutableList<CountryModel>) {
        this.countries.addAll(countries)
        notifyDataSetChanged()
    }


    inner class ViewHolder(private val binding: CountryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var modelCountry: CountryModel

        fun bind() {
            modelCountry = countries[adapterPosition]
            binding.infos = CountryModel(
                modelCountry.name,
                modelCountry.region,
                modelCountry.nativeName,
                modelCountry.flag
            )

            binding.flagIv.loadSvg(modelCountry.flag)
        }

    }


}