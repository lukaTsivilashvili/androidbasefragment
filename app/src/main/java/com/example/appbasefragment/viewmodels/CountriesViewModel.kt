package com.example.appbasefragment.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.appbasefragment.api.RetrofitService
import com.example.appbasefragment.models.CountryModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CountriesViewModel : ViewModel() {


    private val countryLiveData = MutableLiveData<List<CountryModel>>().apply {
        mutableListOf<CountryModel>()
    }

    val _countryLiveData: LiveData<List<CountryModel>> = countryLiveData


    fun init() {
        CoroutineScope(Dispatchers.IO).launch {
            getPhotos()
        }
    }


    private suspend fun getPhotos() {
        val result = RetrofitService.service.getCountry()

        if (result.isSuccessful) {
            val items = result.body()
            countryLiveData.postValue(items)
        }
    }

}