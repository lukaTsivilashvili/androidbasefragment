package com.example.appbasefragment.extensions

import android.net.Uri
import android.widget.ImageView
import com.example.appbasefragment.R
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou

fun ImageView.loadSvg(link: String? = null) {

    if (link.isNullOrBlank()) {

        setImageResource(R.mipmap.ic_launcher)

    } else {

        GlideToVectorYou
            .init()
            .with(context)
            .setPlaceHolder(R.mipmap.ic_launcher_round, R.mipmap.ic_launcher_round)
            .load(Uri.parse(link), this)

    }

}