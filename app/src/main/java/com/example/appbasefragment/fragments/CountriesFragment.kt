package com.example.appbasefragment.fragments

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appbasefragment.adapters.RecyclerAdapter
import com.example.appbasefragment.databinding.CountriesFragmentBinding
import com.example.appbasefragment.viewmodels.CountriesViewModel

class CountriesFragment : BaseFragment<CountriesFragmentBinding, CountriesViewModel>(
    CountriesFragmentBinding::inflate,
    CountriesViewModel::class.java
) {

    private lateinit var myAdapter: RecyclerAdapter

    override fun initialize(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }
    private fun init() {
        viewModel.init()
        initRecycler()
        observe()

    }
    private fun initRecycler() {
        myAdapter = RecyclerAdapter()
        binding.recycler.layoutManager = LinearLayoutManager(requireActivity())
        binding.recycler.adapter = myAdapter
    }
    private fun observe() {

        viewModel._countryLiveData.observe(viewLifecycleOwner, {
            myAdapter.setData(it.toMutableList())
        })
    }
}